# Examples of chaptermark files

Al of the files in this directory describe [episode 111 of the the podcast pythonbytes](https://pythonbytes.fm/episodes/show/111).
They do so in various degrees of accuracy (described in the filename itself).
Therefore they should be good examples for files to merge with chaptermerger.

The original chapters can be seen here or in the file original.chaptermarks:

```
00:00:46.000 Brian #1: loguru: Python logging made (stupidly) simple
00:06:08.000 Michael #2: Python gets a new governance model
00:12:16.000 Brian #3: Why you should be using pathlib
00:16:00.000 Michael #4: Altair and Altair Recipes
00:19:43.000 Brian #5: A couple fun pytest plugins
00:23:23.000 Michael #6: Secure 🔒 headers and cookies for Python web frameworks
00:27:20.000 Extras
```

Some things to look out for when you merge examples:
- there are 7 chapters
- there is an emoji keylock in one of the titles: 🔒
