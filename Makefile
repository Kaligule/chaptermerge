all: unittest typecheck lint regression_test

production_env: requirements/production.txt
	python -m venv production_env
	production_env/bin/pip install -r requirements/production.txt

dev_env: requirements/development.txt
	python -m venv dev_env
	dev_env/bin/pip install -r requirements/development.txt

unittest: dev_env
	dev_env/bin/python -m pytest --cov=chaptermerge --cov-report html

typecheck: dev_env
	dev_env/bin/mypy --package chaptermerge

lint: dev_env
	dev_env/bin/pylint chaptermerge

regression_test: production_env
	bash test/regression_test.bash

clean:
	rm -rf dev_env production_env
