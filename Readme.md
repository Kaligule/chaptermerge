# Chapter merger

chaptermerger is a cli-tool that allows you to merge multiple chapter [mark files](https://auphonic.com/blog/2013/07/08/howto-chapter-marks-auphonic/#mp4chaps) into one, choosing the best timestamps and chaptermarks.
This should allow multiple people write chaptermarks in parallel.
In the end you should get out one chaptermark file with all the best information:

- Someone misses to write down a chapter?
  It will be included as long someone wrote it down.
- Disagree when that chapter started?
  Merge the timestamps using different functions
  (`average`, `median`, `earliest` or the `first` one mentioned)
  (choose with the flag `timestamp_merger`).
- Couldn't decide on a title for the chapter?
  If someone did, then the name will be included - otherwise the chaptername will be left empty.
- Different names for the same chapter?
  You can decide which one to use with the flag `--name_merger`.
-   Use `--max_time_distance` to set the maximum time distance between chapters (in seconds).
  Chaptermarks that are closer then this are going to be merged into one.

Chapter mark files consist of lines formated like this:

## Syntax

```
HH:MM:SS.sss title of the chapter
HH:MM:SS title of the chapter
HH:MM:SS 
```

Given multiple of these files you can merge them like this:

```sh
python -m chaptermerge file_1 file_2 ...
```

You can further specify how timestamps and names should be merged. For details run:

```sh
python -m chaptermerge -h
```

## How to setup

At the moment chaptermerge is not available on pypi (yet).
After cloning/downloading this repository you can setup chaptermerge in a few different ways:

- via nix shell (if you use nix): `nix-shell`
- via virtualenv (if you have python3 already installed): `make production_env; source production_env/bin/activate`
- by globally installing the dependencies (not recommended): `pip install requirements/production.txt`

## References

This tool was favourably discussed in the german _Binaergewitter_ podcast Episode 292
(beginning at [timestamp 14:22](https://blog.binaergewitter.de/2022/03/07/binaergewitter-talk-number-292-usb-autobatterie?t=14%3A22)).