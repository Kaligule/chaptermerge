"""merge multiple chapter-mark files"""

from datetime import timedelta
from typing import List, Tuple

import argparse

from chaptermerge import TimestampMerger, NameMerger
from chaptermerge import timestamp_merger_dict, name_merger_dict
from chaptermerge import cluster_and_merge_chaptermark_lines

parser = argparse.ArgumentParser(description='Some description.')
parser.add_argument('chaptermark files',
                    metavar='chapter_file',
                    nargs='+',  # one or more
                    help='Paths to chapter-mark files that shall be merged.'
                    )
parser.add_argument('--timestamp_merger',
                    choices=timestamp_merger_dict.keys(),
                    default='average',
                    help='How to merge multiple timestamps into one.'
)
parser.add_argument('--name_merger',
                    choices=name_merger_dict.keys(),
                    default='first',
                    help='How to merge multiple names of chapters into one.'
)
parser.add_argument('--max_time_distance',
                    type=int,
                    default='60',
                    help='''Maximum time distance between chapters (in seconds). \
Chaptermarks that are closer then this are going to be merged into one.'''
)


def parse_args() -> Tuple[List[str],
                          TimestampMerger,
                          NameMerger,
                          timedelta
                          ]:
    """Parse the commandline arguments and return them as a tuple"""
    args = parser.parse_args()

    paths_to_chaptermark_files: List[str] = vars(args)['chaptermark files']

    timestamp_merger = timestamp_merger_dict[args.timestamp_merger]

    name_merger: NameMerger = name_merger_dict[args.name_merger]

    # users think in seconds, this programm thinks always in milliseconds
    max_time_distance = timedelta(seconds=args.max_time_distance)

    return paths_to_chaptermark_files, timestamp_merger, name_merger, max_time_distance




def main() -> None:
    """Read chaptermarks from files, cluster and merge them together and print out the result.
    """
    # parse input
    paths_to_chaptermark_files, timestamp_merger, name_merger, max_time_distance = parse_args()
    all_lines = []
    for path in paths_to_chaptermark_files:
        with open(path, 'r', encoding="utf8") as reader:
            all_lines += reader.readlines()
    all_lines = [line.strip() for line in all_lines if line.strip()] # remove empty lines

    # business logic
    new_chaptermark_lines = cluster_and_merge_chaptermark_lines(
        chaptermark_lines=all_lines,
        max_time_distance=max_time_distance,
        timestamp_merger=timestamp_merger,
        name_merger=name_merger
    )

    # output
    for mark in new_chaptermark_lines:
        print(mark)

if __name__ == '__main__':
    main()
