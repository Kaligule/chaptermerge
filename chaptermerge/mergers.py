"""Mergers of Timestamps and Chapternames.

A merger is a function that gets a nonempty list of Xs and returns an X
(not necessarily one of the originals).
"""

from typing import List, Dict, Callable, Optional
from datetime import timedelta
from statistics import mean, median

from chaptermerge import TimestampMerger, NameMerger

def work_on_timestamps(function: Callable[[List[float]], float]) -> TimestampMerger:
    """Take a function that works on floats (returning a float)
    and transform it to work on Timestamps (returning a Timestamp).
    This is done by looking at the timestamps as an amount of seconds
    """
    return lambda timestamps: timedelta(
        seconds=function(
            [ts.total_seconds() for ts in timestamps]
        )
    )

# This would be much cooler by using Haskells Maybe-monad
def work_on_empty_lists(function: Callable[[List[str]], str]) -> NameMerger:
    """If the function resieves an empty List it should return None."""
    def adjusted_function(list_of_strings: List[str]) -> Optional[str]:
        if not list_of_strings:
            return None
        return function(list_of_strings)
    return adjusted_function

# these are the dictionaries where all the known mergers are listed
# (together with a keyword to reference them)
# chaptermerge will only ever work (and test) with mergers referenced in these dicts

timestamp_merger_dict: Dict[str, TimestampMerger] = {
    'earliest': min,
    'average': work_on_timestamps(mean),
    'median': work_on_timestamps(median),
    'first': lambda l: l[0],
}

name_merger_dict: Dict[str, NameMerger] = {
    'first': work_on_empty_lists(lambda l: l[0]),
    'longest': work_on_empty_lists(lambda l: max(l, key=len)),
}
