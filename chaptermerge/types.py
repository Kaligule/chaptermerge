"""Types (and typealiases) used for chaptermerge
"""

from datetime import timedelta
from typing import List, Callable, Optional

Timestamp = timedelta
Chaptername = str
TimestampMerger = Callable[[List[timedelta  ]], timedelta]
NameMerger      = Callable[[List[Chaptername]], Optional[Chaptername]]
