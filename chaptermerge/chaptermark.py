"""Classes used for chaptermerge
"""

# not necessary for python 3.10 or higher, see:
# https://stackoverflow.com/questions/36286894/name-not-defined-in-type-annotation
from __future__ import annotations
from datetime import datetime, timedelta
from typing import Optional

from chaptermerge import Timestamp, Chaptername

class Chaptermark:
    """Represents a single line in a chaptermark file.
    """
    def __init__(self, timestamp: Timestamp, chaptername: Optional[Chaptername]):
        self.timestamp = timestamp
        # We have to distinguish 3 possibillities for the chaptername here:
        if chaptername is None:
            # None -> None
            self.chaptername = None
        elif chaptername.strip():
            # nonempty string -> that, but also striped
            self.chaptername = chaptername.strip()
        else:
            # just whitespace -> None
            self.chaptername = None

    @classmethod
    def from_string(cls, line: str) -> Chaptermark:
        """line should be of the form '00:10:30.000 name of the chapter'"""
        split_on_first_space = line.split(maxsplit=1)
        timestamp_str = split_on_first_space[0]
        if len(split_on_first_space) == 1:
            chaptername = None
        elif len(split_on_first_space) == 2:
            chaptername = split_on_first_space[1]

        try:
            timestamp_as_date = datetime.strptime(timestamp_str, '%H:%M:%S.%f')
        except ValueError:
            timestamp_as_date = datetime.strptime(timestamp_str, '%H:%M:%S')
        timestamp = timedelta(
            hours        = timestamp_as_date.hour,
            minutes      = timestamp_as_date.minute,
            seconds      = timestamp_as_date.second,
            microseconds = timestamp_as_date.microsecond
        )

        return cls(timestamp, chaptername)

    def __str__(self) -> str:

        # if self.chaptername is None then it will not be shown,
        # but there _will_ be a space after the timestamp so it is
        # easy for the reader of a file to add a chaptername
        hours, rest = divmod(self.timestamp, timedelta(hours=1))
        minutes, rest = divmod(rest, timedelta(minutes=1))
        seconds, rest = divmod(rest, timedelta(seconds=1))
        milliseconds = int(rest.total_seconds() * 1000)
        shown_name = "" if self.chaptername is None else self.chaptername
        return f"{hours:02}:{minutes:02}:{seconds:02}.{milliseconds:03} {shown_name}"

    def __lt__(self, other: Chaptermark) -> bool:
        return self.timestamp < other.timestamp
