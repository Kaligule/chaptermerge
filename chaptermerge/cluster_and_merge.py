"""The business logic"""

from datetime import timedelta
from typing import List

from chaptermerge import Chaptermark
from chaptermerge import TimestampMerger, NameMerger


def merge_cluster(cluster: List[Chaptermark],
                  timestamp_merger: TimestampMerger,
                  name_merger: NameMerger
                  ) -> Chaptermark:
    """Merge a cluster of Chaptermarks into a single Chaptermark"""
    return Chaptermark(
        timestamp   = timestamp_merger([mark.timestamp   for mark in cluster]),
        chaptername = name_merger(     [mark.chaptername for mark in cluster
                                        if mark.chaptername is not None])
    )


def find_clusters_by_timestamp(
        chaptermarks: List[Chaptermark],
        max_time_distance: timedelta
        ) -> List[List[Chaptermark]]:
    """If the timestamps of two chaptermarks differ by less then max_time_distance
    then they will end up in the same cluster.
    """
    chaptermarks = sorted(chaptermarks)

    clusters = []
    current_cluster = []
    last_timestamp = None
    for chaptermark in chaptermarks:
        if last_timestamp is None or (chaptermark.timestamp - last_timestamp < max_time_distance):
            # chaptermark belongs in current_cluster
            current_cluster.append(chaptermark)
        else:
            # chaptermark belongs in a new cluster
            clusters.append(current_cluster)
            current_cluster = [chaptermark]
        last_timestamp = chaptermark.timestamp
    # the last cluster might or might not have been added to clusters already
    if current_cluster:
        clusters.append(current_cluster)

    return clusters


def cluster_and_merge_chaptermark_lines(chaptermark_lines: List[str],
                                        max_time_distance: timedelta,
                                        timestamp_merger: TimestampMerger,
                                        name_merger: NameMerger
                                        ) -> List[str]:
    """Wrapper around cluster_and_merge_chaptermarks that works on chaptermark lines
    """
    # parse lines into chaptermark objects
    marks = [Chaptermark.from_string(line) for line in chaptermark_lines]
    new_marks = cluster_and_merge_chaptermarks(marks,
                                               max_time_distance,
                                               timestamp_merger,
                                               name_merger)
    return list(map(str, new_marks))


def cluster_and_merge_chaptermarks(chaptermarks: List[Chaptermark],
                                   max_time_distance: timedelta,
                                   timestamp_merger: TimestampMerger,
                                   name_merger: NameMerger
                                   ) -> List[Chaptermark]:
    """Chaptermarks that may hint to the same chapter are merged together so the result should
    contain only Chaptermarks for different chapters.
    """
    return sorted([
        merge_cluster(cluster, timestamp_merger, name_merger)
        for cluster
        in find_clusters_by_timestamp(chaptermarks, max_time_distance)
    ])
