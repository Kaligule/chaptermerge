{ pkgs ? import <nixpkgs> {} }:

let
    customPython = pkgs.python38.buildEnv.override {
      extraLibs = [ pkgs.python38Packages.ConfigArgParse ];
    };
in

pkgs.mkShell {
  buildInputs = [ customPython ];
}
