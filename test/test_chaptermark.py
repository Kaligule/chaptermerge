from chaptermerge import Chaptermark

from datetime import timedelta
from typing import Tuple, Optional

from hypothesis import given
from hypothesis.strategies import lists, tuples
from custom_strategies import chaptermarks, timestamps, titles

# helper function
def chaptermark_from_tuple(t = Tuple[timedelta, Optional[str]]) -> Chaptermark:
     return Chaptermark(t[0], t[1])

class TestChaptermark:

    @given(chaptermarks())
    def test_write_parse_write_equals_write(self, chaptermark):
        assert str(chaptermark) == str(Chaptermark.from_string(str(chaptermark))), f"""Found counterexample:
Chaptermark(timestamp={chaptermark.timestamp}, chaptername={chaptermark.chaptername})"""

    @given(lists(tuples(timestamps(), titles())))
    def test_chaptermarks_sort_like_their_timestamps(self, tuples_of_timestamps_and_titles):
        chaptermark_list = list(map(chaptermark_from_tuple, tuples_of_timestamps_and_titles))
        sorted_chaptermark_list = sorted(chaptermark_list)

        sorted_tuples = sorted(tuples_of_timestamps_and_titles, key=lambda x: x[0])
        chaptermarks_from_sorted_tuples = list(map(chaptermark_from_tuple, sorted_tuples))

        # chaptermarks can only be compared as strings
        assert list(map(str, sorted_chaptermark_list)) == list(map(str, chaptermarks_from_sorted_tuples))
