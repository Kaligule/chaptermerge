from datetime import timedelta
from chaptermerge import Chaptermark, timestamp_merger_dict, name_merger_dict
from hypothesis.strategies import integers, text, composite, characters, one_of, none, just, sampled_from, booleans

@composite
def timestamps(draw):
    # idea: replace by
    # hypothesis.strategies.timedeltas(max_value=(timedelta(hours=24)-timedelta(milliseconds=1))) 
    hours        = draw(integers(min_value=0, max_value=   24-1))
    minutes      = draw(integers(min_value=0, max_value=   60-1))
    seconds      = draw(integers(min_value=0, max_value=   60-1))
    milliseconds = draw(integers(min_value=0, max_value= 1000-1))
    return timedelta(hours=hours, minutes=minutes, seconds=seconds, milliseconds=milliseconds)

@composite
def timestamp_strings(draw):
    hours        = draw(integers(min_value=0, max_value=   24-1))
    minutes      = draw(integers(min_value=0, max_value=   60-1))
    seconds      = draw(integers(min_value=0, max_value=   60-1))
    with_milliseconds = draw(booleans())
    if with_milliseconds:
        milliseconds = draw(integers(min_value=0, max_value= 1000-1))
        timestamp_string = f"{hours:02}:{minutes:02}:{seconds:02}.{milliseconds:03}"
    else:
        timestamp_string = f"{hours:02}:{minutes:02}:{seconds:02}"
    return timestamp_string

@composite
def titles(draw):
    return draw(text(
        min_size=1,
        # Don't allow characters from the category "other"
        # https://en.wikipedia.org/wiki/Unicode_character_property#General_Category
        alphabet=characters(blacklist_categories=('C'))
    ))

@composite
def chaptermark_strings(draw):
    timestamp_string = draw(timestamp_strings())
    title = draw(one_of(
         none(),    # No title
         just(""),  # No title but a trailing space
         titles()   # a nonempty title, sparated from the timestamp by a space
    ))
    if title is None:
         return f"{timestamp_string}"
    else:
         return f"{timestamp_string} {title}"

@composite
def chaptermarks(draw):
    timestamp = draw(timestamps())
    title = draw(titles())
    return Chaptermark(timestamp=timestamp, chaptername=title)

# Hypothesis goes to some length to ensure that the sampled_from
# strategy has stable results between runs. To replay a saved example,
# the sampled values must have the same iteration order on every run -
# ruling out sets, dicts, etc due to hash randomisation.
# That is why these lists are put here and not in the strategies-functions.
all_timestamp_mergers = list(timestamp_merger_dict.values())
all_name_mergers      = list(name_merger_dict.values())

@composite
def timestamp_mergers(draw):
    return draw(sampled_from(all_timestamp_mergers))

@composite
def name_mergers(draw):
    return draw(sampled_from(all_name_mergers))
