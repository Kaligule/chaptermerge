Each "*.test" file represents a regression test.

They have the following format:

```
bash command

expected output
of that bash
command
```
