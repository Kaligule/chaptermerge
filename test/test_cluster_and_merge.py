from chaptermerge import cluster_and_merge_chaptermarks, cluster_and_merge_chaptermark_lines

from hypothesis import given
from hypothesis.strategies import lists, timedeltas
from custom_strategies import chaptermarks, chaptermark_strings, timestamp_mergers, name_mergers

class TestClusterAndMerge:

    @given(lists(chaptermarks()),
           timedeltas(),
           timestamp_mergers(),
           name_mergers(),
           )
    def test_cluster_and_merge_chaptermarks_makes_lists_shorter(
            self,
            list_of_chaptermarks,
            max_time_distance,
            timestamp_merger,
            name_merger):
        clustered_and_shrank = cluster_and_merge_chaptermarks(
            list_of_chaptermarks,
            max_time_distance,
            timestamp_merger,
            name_merger)
        assert len(clustered_and_shrank) <= len(list_of_chaptermarks)

    @given(lists(chaptermark_strings()),
           timedeltas(),
           timestamp_mergers(),
           name_mergers(),
           )
    def test_cluster_and_merge_chaptermark_lines_makes_lists_shorter(
            self,
            list_of_chaptermark_lines,
            max_time_distance,
            timestamp_merger,
            name_merger):
        clustered_and_shrank = cluster_and_merge_chaptermark_lines(
            list_of_chaptermark_lines,
            max_time_distance,
            timestamp_merger,
            name_merger)
        assert len(clustered_and_shrank) <= len(list_of_chaptermark_lines)
