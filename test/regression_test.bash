source production_env/bin/activate

exit_code=0
while read testfile; do
    echo "# regression_test $testfile"
    command=`head -n 1 $testfile`
    expected_output=`tail -n +3 $testfile`
    real_output=`$command`
    # shell substitution doesn't work in sh, sadly
    # Thats why this line only works in bash (or zsh etc)
    diff <(echo "$expected_output") <(echo "$real_output")

    last_exit_code="$?"
    if [ $last_exit_code != 0 ]; then
        echo "Output not as expected for testfile $testfile"
        exit_code=1
    fi
done <<< $(find test/regression_tests/ -name "*.test")

exit $exit_code
x